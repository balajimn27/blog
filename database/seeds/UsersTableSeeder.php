<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(array(
            'email' => 'balaji27@email.com',
            'password' => Hash::make('password'),
            'name' => 'balaji'
        ));
    }
}
